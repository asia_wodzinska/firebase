import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";

import {
  getFirestore,
  addDoc,
  collection,
  setDoc,
  updateDoc,
  deleteDoc,
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js";

import {
  getAuth,
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  signOut,
  onAuthStateChanged,
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

const firebaseConfig = {
  apiKey: "AIzaSyACXwhrYpUv6BG7jjbMff67R3Ti4V3wdvw",
  authDomain: "fir-application-6d29a.firebaseapp.com",
  databaseURL:
    "https://fir-arpfrontend-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "fir-arpfrontend",
  storageBucket: "fir-arpfrontend.appspot.com",
  messagingSenderId: "41470028575",
  appId: "1:41470028575:web:1750c7e473a10e3cedde2a",
  measurementId: "G-YJNX62F21F",
};

const formEmail = document.querySelector("#formEmail");
const formPassword = document.querySelector("#formPassword");
const firstName = document.querySelector("#firstName");
const lastName = document.querySelector("#lastName");
const country = document.querySelector("#country");
const btnLogin = document.querySelector("#btnLogin");
const btnSignup = document.querySelector("#btnSignup");
const btnRegister = document.querySelector("#btnRegister");
const btnLogout = document.querySelector("#btnLogout");
const lblErrorMessage = document.querySelector("#lblErrorMessage");
const registerNewUser = document.querySelector("#registerNewUser");

const app = initializeApp(firebaseConfig);

// log in
const loginUser = async () => {
  const valueEmail = formEmail.value;
  const valuePassword = formPassword.value;

  try {
    const user = await signInWithEmailAndPassword(
      auth,
      valueEmail,
      valuePassword
    );
    console.log("You have logged in successfully");
  } catch (error) {
    lblErrorMessage.innerHTML = error;
  }
};

btnLogin.addEventListener("click", loginUser);

// register form disply
const displayForm = () => {
  registerNewUser.style.display = "none";
};
displayForm();

function registerFormForNewUser() {
  if (btnRegister) {
    btnLogin.style.display = "none";
    btnRegister.style.display = "none";
    registerNewUser.style.display = "block";
  }
}
btnRegister.addEventListener("click", registerFormForNewUser);

// register

const db = getFirestore(app);
const auth = getAuth(app);


const createUser = async () => {
    const valueEmail = formEmail.value;
    const valuePassword = formPassword.value;
    try {
      const user = await createUserWithEmailAndPassword(
        auth,
        valueEmail,
        valuePassword
      );
      console.log(user.user);
    } catch (error) {
      console.warn(error);
    }
  };
  
  btnSignup.addEventListener("click", createUser);


const email = formEmail.value;
const userRef = collection(db, "users", email);

const addNewUser = {
  email: formEmail.innerHTML,
  name: firstName.innerHTML,
  lastName: lastName.innerHTML,
  fromCuntry: country.innerHTML
};
addDoc(userRef, addNewUser);
btnSignup.addEventListener("click", createUser);
btnSignup.addEventListener("click", addNewUser);

// log out
async function logoutUser() {
  await signOut(auth);
  console.log("You have logged out successfully");
}

btnLogout.addEventListener("click", logoutUser);

const authUserStatus = async () => {
  onAuthStateChanged(auth, (user) => {
    if (user) {
      loginForm.style.display = "none";
      userAuth.style.display = "block";
      registerNewUser.style.display = "none";
    } else {
      loginForm.style.display = "block";
      userAuth.style.display = "none";
    }
  });
};
authUserStatus();
