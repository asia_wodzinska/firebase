// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
import {
  getFirestore,
  getDoc,
  doc,
  setDoc, 
  updateDoc,
  deleteDoc
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-firestore.js";

const firebaseConfig = {
  apiKey: "AIzaSyBkO7oRYBSP7DRUCrKFlbBRf5DnbSjD97s",
  authDomain: "fir-arpfrontend.firebaseapp.com",
  projectId: "fir-arpfrontend",
  storageBucket: "fir-arpfrontend.appspot.com",
  messagingSenderId: "633533272223",
  appId: "1:633533272223:web:a89fcb021446dfd9d052aa",
  measurementId: "G-VL6RVDX9RD",
};

// ODCZYT
const appOne = initializeApp(firebaseConfig);
const db = getFirestore(appOne);

const docRef = doc(db, "movie", "1");
const docSnap = await getDoc(docRef);

if (docSnap.exists()) {
  console.log(docSnap.data());
} else {
  console.log("This move does not exist :(");
}

// ZAPIS

const movieRef = doc(db, "movie", "4");

const addMovie = {
  avalible: true,
  country: "USA",
  title: "Fight Club",
  year_of_production: 1999,
};

setDoc(movieRef, addMovie);


//  AKTUALIZACJA

const movieForUpdateRef = doc(db, "movie", "1");

const movieForUpdate = {
    avalible: false,
    country: "South Korea",
    title: "Oldboy",
    year_of_production: 2003,
};

//await updateDoc(movieForUpdateRef, movieForUpdate);

// USUN
// await deleteDoc(doc(db, "movie", "1"));


