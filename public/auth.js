// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js";
import {
  getAuth,
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  signOut,
  onAuthStateChanged,
} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";

const firebaseConfig = {
  apiKey: "AIzaSyBkO7oRYBSP7DRUCrKFlbBRf5DnbSjD97s",
  authDomain: "fir-arpfrontend.firebaseapp.com",
  projectId: "fir-arpfrontend",
  storageBucket: "fir-arpfrontend.appspot.com",
  messagingSenderId: "633533272223",
  appId: "1:633533272223:web:a89fcb021446dfd9d052aa",
  measurementId: "G-VL6RVDX9RD",
};

const app = initializeApp(firebaseConfig);

const auth = getAuth(app);

const formEmail = document.querySelector("#formEmail");
const formPassword = document.querySelector("#formPassword");
const btnLogin = document.querySelector("#btnLogin");
const btnSignup = document.querySelector("#btnSignup");
const btnLogout = document.querySelector("#btnLogout");
const lblErrorMessage = document.querySelector("#lblErrorMessage");

// 1 LOGOWANIE

const loginUser = async () => {
  const valueEmail = formEmail.value;
  const valuePassword = formPassword.value;

  try {
    const user = await signInWithEmailAndPassword(
      auth,
      valueEmail,
      valuePassword
    );
    console.log("You have logged in successfully")
  } catch (error) {
    lblErrorMessage.innerHTML = error;
  }
};

btnLogin.addEventListener("click", loginUser);

// 2 REJESTRACJA

const createUser = async () => {
  const valueEmail = formEmail.value;
  const valuePassword = formPassword.value;
  try {
    const user = await createUserWithEmailAndPassword(
      auth,
      valueEmail,
      valuePassword
    );
    console.log(user.user);
  } catch (error) {
    console.warn(error);
  }
};

btnSignup.addEventListener("click", createUser);

// 3 WYLOGOWANIE

const logoutUser = async () => {
  await signOut(auth);
  console.log("You have logged out successfully");
};

btnLogout.addEventListener("click", logoutUser);


const authUserStatus = async() => {
  onAuthStateChanged(auth, user => {
    if (user) {
loginForm.style.display = "none"
userAuth.style.display = "block"
    } else {
      loginForm.style.display = "block"
userAuth.style.display = "none"
    }
});
}

authUserStatus();


